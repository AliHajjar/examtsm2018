import csv

with open('../Data/Coffeebar_2013-2017.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    times=[]
    costumers=[]
    drinks=[]
    foods=[]

    for row in readCSV:
        time=row[0]
        costumer=row[1]
        drink=row[2]
        food=row[3]

        times.append(time)
        costumers.append(costumer)
        drinks.append(drink)
        foods.append(food)


#Create new list with only the time in dates
hours=[]
for i in times:
    hours.append(i[-8:])

hourslist=list(sorted(set(hours[1:])))   #1: is to remove the title from data

#creating costumers:

#Importing probas, this in important for step 3!
with open('../Data/probadrinks.csv') as csvfile:
    readCSV1 = csv.reader(csvfile, delimiter=';')

    dates=[]
    pcoffee=[]
    pfrappucino=[]
    pmilkshake=[]
    psoda=[]
    ptea=[]
    pwater=[]

    for row in readCSV1:
        date1 = row[1]
        coffee1 = row[2]
        frappucino1 = row[3]
        milkshake1 = row[4]
        soda1 = row[5]
        tea1 = row[6]
        water1 = row[7]
# These are lists with drink's probabilities for each date(time)
        dates.append(date1)
        pcoffee.append(coffee1)
        pfrappucino.append(frappucino1)
        pmilkshake.append(milkshake1)
        psoda.append(soda1)
        ptea.append(tea1)
        pwater.append(water1)

with open('../Data/probafoods.csv') as csvfile:
    readCSV1 = csv.reader(csvfile, delimiter=';')

    pcookie=[]
    pmuffin=[]
    pnothing=[]
    ppie=[]
    psandwich=[]

    for row in readCSV1:
        cookie1 = row[2]
        muffin1 = row[3]
        nothing1 = row[4]
        pie1 = row[5]
        sandwich1 = row[6]

# These are lists with food's probabilities for each date(time)
        pcookie.append(cookie1)
        pmuffin.append(muffin1)
        pnothing.append(nothing1)
        ppie.append(pie1)
        psandwich.append(sandwich1)

#A costumer will come at a randome time between the 170 time possibilities and will consume randomly a drik or/and food depending on the proba on the time where she came
class unique(object):
    def __init__(self, unique_budget, unique_tip):
        self.budget=unique_budget
        self.tip=unique_tip


class returning(object):
    def __init__(self, returning_budget, returning_tip):
        self.budget=returning_budget
        self.tip=returning_tip

#I chose to only include tip and budget in this step as I have parts 2 and 3 kind of mixed
import numpy as np
import random

#Give attributes to each type of consumer, including a time and a food/drink
unique_regular=unique(100, 0)
unique_trip=unique(100,random.randint(1,10))  #The tip is randomly picked between  1 and 10
returning_regular=returning(250, 0)
returning_hipster=returning(500,0)


#Simulation (Requires attention)

#1st step : Creation of 1000 returning costumers: Starting with IDs
returningIDS=[]
for i in range(666): #Regural returning
    returningIDS.append("IDR" + str(i))
for i in range(334): #Hipsters returning
    returningIDS.append("IDH" + str(i))
#There are 334 Hipster (1/3) and 666 regulars (2/3)
#Now we can see that the list already incorportates the probability of having a hipster or regular returning client

#Each returning costumer has now an ID and a budget attributed, we do not care about the tip since it is 0 for ALL
budgets=[]
for i in range(666):
    budgets.append(returning_regular.budget)
for i in range(334):
    budgets.append(returning_hipster.budget)

#buyings is to keep record of all what has been purchased by each returning costumer
buyings=[]
for i in range(1000):
    buyings.append("Hisotry: ")

#Totalpaid is for saving how much each of the returning costumers have spent
totalpaid=[]
for i in range(1000):
    totalpaid.append(0)   #At the begining it is 0 for each since they didn't cosume anything yet



#Now for the final table we will creat the corresponding variables
#These would be the columns for each variable in the final table

finaltime=[]
finaltimeindex=[]  #the indexes from 1 to 170 are used to be able to compute easily probas (Note that 170 are the number of possible dates)
finalID=[]
finaldrinks=[]
finalfood=[]
finalcashflow=[]  #Finalcashflow is to keep track of how much it is gained per day

#We simulate over 5 years so we have 1825 days !
for i in range(1825):
    for z in hourslist[1:]:
        finaltime.append(z)  #We append the dates to the final list
    for k in range(1,171):
        finaltimeindex.append(k) #We append the index for each date


#When deleting a costumer that can no longer buy, we should save its ID and budget to add it back to the list later.
#Because in ou program if a costumer has no longer money to buy she will be deleted from the list but we don't want to lose her history so we keep it in those lists and recover them later
lostID=[]
lostbudget=[]
lostbuyings=[]
losttotalpaid=[]

#Useful variables for statistics/ to count how much of each is consumed/ And what was the best hour ever
cookie=0
muffin=0
pie=0
sandwich=0
tea=0
coffee=0
milkshake=0
soda=0
water=0
frappucino=0
hour=0


# Now that everything is set, we will start the simulation

lenght = 999 #Is for the loop you will understand more how it works later (999 returning costumers with 0 included =1000)

for i in range(len(finaltime)): #For each date of the 1825
    totalfood=0 #To count the Cashflows of food
    totaldrink=0 #To count the Cashflows of drinks

    if len(returningIDS)>0:  #If there exist returning client with a budget higher than 10

        r = random.randint(0, 101) #101 because the upper range is not taken into cosideration
        #So we draw a random number between 1 and 100: We have 20% proba of having a number lower or equal to 20...
        #20% is the proba of having a returning costumer

        if r <=20:  #If r<20 then a random returning costumer is chosen from the list

            a = random.randint(-1, lenght) #randomly between 0 and 999

            finalID.append(returningIDS[a])  # FinalID takes the ID corresponding randomly

            #In this step we will for each date chose, we give the corresponding food or drink given the probas
            prob=finaltimeindex[i]  #Each of the 170 time possibility has its owns probas (the use of the index)
            pfood = np.random.choice(
                ['Cookie', 'Muffin', 'Nothing', 'Pie', 'Sandwich'],
                p=[pcookie[prob], pmuffin[prob], pnothing[prob], ppie[prob], psandwich[prob]]
            )
            pdrink = np.random.choice(
                ['Coffee', 'Frappucino', 'Milkshake', 'Soda', 'Tea', 'Water'],
                p=[pcoffee[prob], pfrappucino[prob], pmilkshake[prob], psoda[prob], ptea[prob], pwater[prob]]
            )

            finalfood.append(pfood)
            finaldrinks.append(pdrink)

            if finalfood[i]=="Cookie": #If the proba allowed to have Cookie, the costumer will buy a cookie
                totalfood+=2 #This is what she paid for it
                cookie=cookie+1 #This is to count how many of cookies were bought !
            elif finalfood[i]=="Sandwich":
                totalfood+=5
                sandwich=sandwich+1
            elif finalfood[i]=="Nothing":
                totalfood+=0
            elif finalfood[i]=="Pie":
                totalfood+=3
                pie=pie+1
            elif finalfood[i]=="Muffin":
                totalfood += 3
                muffin=muffin+1

            if finaldrinks[i]== "Milkshake":
                totaldrink+=5
                milkshake=milkshake+1
            elif finaldrinks[i] == "Frappucino":
                totaldrink+=4
                frappucino=frappucino+1
            elif finaldrinks[i] == "Water":
                totaldrink+=2
                water=water+1
            elif finaldrinks[i] =="Tea":
                tea=tea+1
                totaldrink+=3
            elif finaldrinks[i] =="Coffee":
                coffee=coffee+1
                totaldrink += 3
            elif finaldrinks[i] =="Soda":
                soda=soda+1
                totaldrink += 3

            finalcashflow.append(totaldrink+totalfood)

            budgets[a]=budgets[a]-finalcashflow[i]  #The chosen client will have her budget cut out from what she baught

            buyings[a]=buyings[a]+ " " + str(finaltime[i]) + " :" +str(pfood)+ "/ "+str(pdrink) #This is to keep record of the buyings of each client

            totalpaid[a]=totalpaid[a]+totaldrink+totalfood #Sum of how much the costumer has paid up to the end

            if budgets[a]<10:  #If the costumer has no longer money to buy, she will be removed from the list in order to not be picked anymore
                #But before deleting her, we keep all her informations saved in the Lost lists !!
                lostID.append(returningIDS[a])
                losttotalpaid.append((totalpaid[a]))
                lostbudget.append((budgets[a]))
                lostbuyings.append((buyings[a]))
                returningIDS.remove(returningIDS[a])
                budgets.remove(budgets[a])
                buyings.remove(buyings[a])
                totalpaid.remove(totalpaid[a])
                lenght=lenght-1 #There will be a costumer removed...

        else: #If r>20% -> A non returning is chosen instead

            finalID.append("IDU" + str(i)) #He has the Id : IDU plus the index i (For each to have a unique ID)

            # Proba, as shown earlier
            prob = finaltimeindex[i]
            pfood = np.random.choice(
                ['Cookie', 'Muffin', 'Nothing', 'Pie', 'Sandwich'],
                p=[pcookie[prob], pmuffin[prob], pnothing[prob], ppie[prob], psandwich[prob]]
            )
            pdrink = np.random.choice(
                ['Coffee', 'Frappucino', 'Milkshake', 'Soda', 'Tea', 'Water'],
                p=[pcoffee[prob], pfrappucino[prob], pmilkshake[prob], psoda[prob], ptea[prob], pwater[prob]]
            )

            finalfood.append(pfood)
            finaldrinks.append(pdrink)
            if finalfood[i] == "Cookie":
                totalfood += 2
                cookie = cookie + 1
            elif finalfood[i] == "Sandwich":
                totalfood += 5
                sandwich = sandwich + 1
            elif finalfood[i] == "Nothing":
                totalfood += 0
            elif finalfood[i] == "Pie":
                totalfood += 3
                pie = pie + 1
            elif finalfood[i] == "Muffin":
                totalfood += 3
                muffin = muffin + 1

            if finaldrinks[i] == "Milkshake":
                totaldrink += 5
                milkshake = milkshake + 1
            elif finaldrinks[i] == "Frappucino":
                totaldrink += 4
                frappucino = frappucino + 1
            elif finaldrinks[i] == "Water":
                totaldrink += 2
                water = water + 1
            elif finaldrinks[i] == "Tea":
                tea = tea + 1
                totaldrink += 3
            elif finaldrinks[i] == "Coffee":
                coffee = coffee + 1
                totaldrink += 3
            elif finaldrinks[i] == "Soda":
                soda = soda + 1
                totaldrink += 3

            y=random.randint(0,101) #To determine whether it is tripadvisor client or not
            if y<=10: #Same logic, 10% proba to have 10 or lower
                finalcashflow.append(totaldrink + totalfood)
            else:
                tip=random.randint(1,11)
                finalcashflow.append(totaldrink + totalfood+ unique_trip.tip)  #to take tip


    elif len(returningIDS)==0:  #If there are no longer returning costumes with a budget !!
        #We only take into consideration the unique costumers in this case
        #Very same process
        finalID.append("IDU" + str(i))

        prob = finaltimeindex[i]
        pfood = np.random.choice(
            ['Cookie', 'Muffin', 'Nothing', 'Pie', 'Sandwich'],
            p=[pcookie[prob], pmuffin[prob], pnothing[prob], ppie[prob], psandwich[prob]]
        )
        pdrink = np.random.choice(
            ['Coffee', 'Frappucino', 'Milkshake', 'Soda', 'Tea', 'Water'],
            p=[pcoffee[prob], pfrappucino[prob], pmilkshake[prob], psoda[prob], ptea[prob], pwater[prob]]
        )

        finalfood.append(pfood)
        finaldrinks.append(pdrink)
        if finalfood[i] == "Cookie":
            totalfood += 2
            cookie = cookie + 1
        elif finalfood[i] == "Sandwich":
            totalfood += 5
            sandwich = sandwich + 1
        elif finalfood[i] == "Nothing":
            totalfood += 0
        elif finalfood[i] == "Pie":
            totalfood += 3
            pie = pie + 1
        elif finalfood[i] == "Muffin":
            totalfood += 3
            muffin = muffin + 1

        if finaldrinks[i] == "Milkshake":
            totaldrink += 5
            milkshake = milkshake + 1
        elif finaldrinks[i] == "Frappucino":
            totaldrink += 4
            frappucino = frappucino + 1
        elif finaldrinks[i] == "Water":
            totaldrink += 2
            water = water + 1
        elif finaldrinks[i] == "Tea":
            tea = tea + 1
            totaldrink += 3
        elif finaldrinks[i] == "Coffee":
            coffee = coffee + 1
            totaldrink += 3
        elif finaldrinks[i] == "Soda":
            soda = soda + 1
            totaldrink += 3

        y = random.randint(1, 101)
        if y <= 10:
            finalcashflow.append(totaldrink + totalfood)
        else:
            finalcashflow.append(totaldrink + totalfood + unique_trip.tip)

#After the simulation finished, we add the deleted costumers(And their data) to the normal list
returningIDS.extend(lostID)
budgets.extend(lostbudget)
buyings.extend(lostbuyings)
totalpaid.extend((losttotalpaid))

#Now we use Panda to create a table with all the simulation
import pandas as pd
finaltable=pd.DataFrame.from_dict({"..Time":finaltime,   # The dot is for time to be 1st since it is ordered
                    ".Client" :finalID,
                    ".Food":finalfood,
                    ".Drink":finaldrinks,
                    "Cashflows":finalcashflow,
})

#Table that keep track of costumers history
historic=pd.DataFrame.from_dict({"..Costumer":returningIDS,   # The dot is for time to be 1st since it is ordered
                    ".Left Budget" :budgets,
                    ".Total Paid": totalpaid,
                    "Buyings":buyings,

})



# PLOTS !

#Sum per year Cashflows

SCF1=0
SCF2=0
SCF3=0
SCF4=0
SCF5=0

for i in range(365):
    SCF1=SCF1+finalcashflow[i]
for i in range(366,731):
    SCF2=SCF2+finalcashflow[i]
for i in range(731,1096):
    SCF3=SCF3+finalcashflow[i]
for i in range(1096,1461):
    SCF4=SCF4+finalcashflow[i]
for i in range(1461,1826):
    SCF5=SCF5+finalcashflow[i]

import matplotlib.pyplot as plt
y = [SCF1, SCF2, SCF3, SCF4, SCF5]
x = ["Year 1","Year 2","Year 3","Year 4","Year 5"]
plt.bar(x, y, color="Red")
plt.title("Cashflows per year")
plt.show()

#Average per day each year Cashflows
#for the graph
ACF1=SCF1/365
ACF2=SCF2/365
ACF3=SCF3/365
ACF4=SCF4/365
ACF5=SCF5/365

import matplotlib.pyplot as plot

y = [ACF1, ACF2, ACF3, ACF4, ACF5]
x = ["Year 1","Year 2","Year 3","Year 4","Year 5"]
plot.bar(x, y, color="Red")
plot.title("Average daily Cashflows per year")
plot.show()

#Plot of number of drink/food consumed for statistics
y = [cookie, muffin, pie, sandwich]
x = ["Cookie","Muffin","Pie","Sandwich"]
plot.bar(x, y, color="blue")
plot.title("Number of food")
plot.show()

y = [coffee, frappucino, milkshake, soda,tea,water]
x = ["Coffee","Frappucino","Milkshake","Soda","Tea","Water"]
plot.bar(x, y, color="blue")
plot.title("Number of drinks")
plot.show()


#Table with how much were consumed

product=["Cookie","Pie","Muffin","Sandwich","Water","Soda","Frappuciono","Coffee","Milkshake","Tea"]
productC=[cookie,pie,muffin,sandwich,water,soda,frappucino,coffee,milkshake,tea]

consumption=pd.DataFrame.from_dict({"Product":product,
                    "Sum":productC,
})

print(finaltable.describe())

#Part 4:   Some Elements to respond to the Part 4 situations

#Client history:

print(returningIDS[43],budgets[43],totalpaid[43],buyings[43])

counter_regular=0
counter_hipster=0
counter_unique=0
for i in finalID: #To count how many time returning costumers have come (IDR for regulars IDH for hipsters)
    if i[:3]=="IDH":
        counter_hipster+=1
    if i[:3] == "IDR":
        counter_regular+=1
    if i[:3]=="IDU":
        counter_unique+=1


print(counter_hipster)
print(counter_regular)
print(counter_unique)

#Proba of showing up each time
#Recall: Each time has an index from 1 to 170
proba_returning=[]
proba_unique=[]
count_index=[]
count_proba=[]
for i in range(len(dates)):
    proba_returning.append(0)
    proba_unique.append(0)
    count_index.append(0)
    count_proba.append(0)
for i in range(len(finaltime)):
    count_index[int(finaltimeindex[i])] += 1
    if finalID[i][:3]=="IDH" or finalID[i][:3]=="IDR":
         count_proba[int(finaltimeindex[i])] +=1

for i in range(1,170):
    proba_returning[i]=count_proba[i]/count_index[i]
    proba_unique[i]=1-proba_returning[i]



print(sum(finalcashflow))


