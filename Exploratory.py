import csv

with open('../examtsm2018/Data/Coffeebar_2013-2017.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    times=[]
    costumers=[]
    drinks=[]
    foods=[]

    for row in readCSV:
        time=row[0]
        costumer=row[1]
        drink=row[2]
        food=row[3]

        times.append(time)
        costumers.append(costumer)
        drinks.append(drink)
        foods.append(food)
#Question 1
drinksname=list(sorted(set(drinks[1:]))) #We put 1: to not take into account the title row
foodsname=list(sorted(set(foods[1:])))


costumerslist=list(set(costumers[1:])) #All the costumers that came (we count each costumer once)
ncostumers=len(costumerslist) #We count the number of costumers

#For the probas we want to count how many of each type was consumed during the 5 years
#Drinks

Nwater=0
for i in drinks:
    if i=="water":
      Nwater +=1

Ntea=0
for i in drinks:
    if i=="tea":
      Ntea +=1

Nfrappucino=0
for i in drinks:
    if i=="frappucino":
      Nfrappucino +=1

Nsoda=0
for i in drinks:
    if i=="soda":
      Nsoda +=1

Ncoffee=0
for i in drinks:
    if i=="coffee":
      Ncoffee +=1

Nmilkshake=0
for i in drinks:
    if i=="milkshake":
      Nmilkshake +=1



#Foods

Npie=0
for i in foods:
    if i=="pie":
      Npie +=1

Nsandwich=0
for i in foods:
    if i=="sandwich":
      Nsandwich +=1

Ncookie=0
for i in foods:
    if i=="cookie":
      Ncookie +=1

Nmuffin=0
for i in foods:
    if i=="muffin":
      Nmuffin +=1

#Create new list with only the time in dates
hours=[]
for i in times:
    hours.append(i[-8:])

hourslist=list(sorted(set(hours[1:])))   #1: is to remove the title from data

#Proba table per time for each drink

import pandas as pd
import numpy as np

pcoffee=[]
pfrappucino=[]
ptea=[]
pmilkshake=[]
psoda=[]
pwater=[]

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if drinks[i]=="coffee":
                p +=1
    prb=p/n
    pcoffee.append(prb)

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if drinks[i]=="tea":
                p +=1
    prb=p/n
    ptea.append(prb)

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if drinks[i]=="frappucino":
                p +=1
    prb=p/n
    pfrappucino.append(prb)

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if drinks[i]=="soda":
                p +=1
    prb=p/n
    psoda.append(prb)

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if drinks[i]=="milkshake":
                p +=1
    prb=p/n
    pmilkshake.append(prb)

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if drinks[i]=="water":
                p +=1
    prb=p/n
    pwater.append(prb)




probadrinks=pd.DataFrame.from_dict({".Time":hourslist,   # The dot is for time to be 1st since it is ordered
                    "Coffee":pcoffee,
                    "Tea":ptea,
                    "Frappucino":pfrappucino,
                    "Soda":psoda,
                    "Water":pwater,
                    "Milkshake":pmilkshake,
})


print(probadrinks)

ppie=[]
pcookie=[]
pmuffin=[]
psandwich=[]
pnothing=[]

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if foods[i]=="pie":
                p +=1
    prb=p/n
    ppie.append(prb)

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if foods[i]=="sandwich":
                p +=1
    prb=p/n
    psandwich.append(prb)

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if foods[i]=="muffin":
                p +=1
    prb=p/n
    pmuffin.append(prb)

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if foods[i]=="":
                p +=1
    prb=p/n
    pnothing.append(prb)

for c in range(len(hourslist)):
    n= 0
    p=0
    prb=0
    for i in range(len(times)):
        if times[i][-8:]==hourslist[c]:
            n +=1
            if foods[i]=="cookie":
                p +=1
    prb=p/n
    pcookie.append(prb)


probafoods=pd.DataFrame({".Time":hourslist,   # The dot is for time to be 1st since it is ordered
                    "Pie":ppie,
                    "Cookie":pcookie,
                    "Muffin":pmuffin,
                    "Sandwich":psandwich,
                    "Nothing":pnothing
})


print(probafoods)

probadrinks.to_csv('probadrinks.csv', sep=';')
probafoods.to_csv('probafoods.csv', sep=';')

#Plotings

# Plot Number of drinks
import matplotlib.pyplot as plt
y = [Nwater, Ntea, Nfrappucino, Nsoda, Ncoffee, Nmilkshake]
x = ["Water","Tea","Frappucino","Soda","Coffee","Milkshake"]
plt.bar(x, y, color="blue")
plt.title("Number of drinks")
plt.show()



# Plot Number of food
import matplotlib.pyplot as plot
y = [Ncookie, Npie, Nsandwich, Nmuffin]
x = ["Cookie","Pie","Sandwich","Muffin"]
plot.bar(x, y, color="blue")
plt.title("Number of food")
plot.show()
