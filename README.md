# README #



### What is this repository for? ###

* You will find our ExamTSM2018 project by Firas Dayekh and Ali Hajjar

### How do I get set up? ###

* We tried to be as explicit as possible on out code using comments, however for more clarification and to facilitate the understanding of the logic and the complex point we tried to detail everything on our project
* You should note that some "print", "plt.show" ... intructions have been deleted from the code in order to give you the freedom to print whatever variable you want and in order to make the code easier to read.
* Note that all the final and important results that come in the table form have been uploaded in csv files that you may find in the "Data" folder.
* If still, you can't run the code, don't understand the logic or have questions don't hesitate to contact us for clarification.

* ENJOY READING US